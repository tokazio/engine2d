# JAVA(fx) 2D ENGINE #
Simple 2D engine (main loop targeting 60fps with openGL + keyboard inputs handling).

Basically made for tile engine / sprite engine to create top down RPG game.

Work in progress.

### Core ###

* TileSet loader (from file)
* Sprite/AnimatedSprite (based on a TileSet with keyframes)
* JavaFx 2D Canvas engine
* Map loading (TMX)

### TileSet ###
Load a tile set from file in one Image.
All tiles got the same size and numbered from top-left to bottom-right by rows like:
0 1 2
3 4 5
6 7 8
You can set a transparent color from a point or by giving a color value.
One Image is used the only the interesting part (drawImage with clipping) is rendered by its tile code.

### Tile ###
A general tile is defined by it's size and position.

### Sprites ###
Sprite are based on a tile set.
Animated sprite got keyframes from the tile set.
For example: [3,4,5] will animate tiles #3, #4, #5 in a giving time (fixed).
It can be looped, from the whole key frames or a part.
For example: 3,4,5 then loop from the second key: 4,5,4,5...
You can ask for a minimum number of playing keys.
For example: 2 keys at least.

### Canvas engine ###
The engine is setting the scene with a canvas and using JavaFX AnimationTimer.
It call an initialize method after the scene was created and before the main loop.
It call an update method in the main loop.
AnimationTimer is targeting 60fps.
It handle keys with a observer/observable design pattern. Keyables must be added to the engine list to be handled.
Collision are base on a per tile definition from the map.

### Map ###

TileSet based map from TMX file.
Tiles with collisions definition.
With multi layers having its own render event, you can handle z-index drawing.
Only CSV data are supported for now.

### Example ###

See 'demo' package for more examples.

```
#!java
//Loading a tile set and set the color @ 0,0 to be transparent
	TileSet t = new TileSet("chars.png", 32, 32).setTransparent(0, 0);
	//New fixed sprite @ 0,0 using the tile #1 from the tile set
	Sprite sprite = new Sprite(t,1,0,0);	    
	//New animated sprite @ 40,40 using the tile set
        AnimatedSprite aSprite = new AnimatedSprite(t,40,40)
            //set animation speed
            .setDuration(250)
            //adding keyframes animation using tiles 4,5,4,3 in a animation called down
            .addKeyFrames("DOWN", new KeyFrames(4, 5, 4, 3))
	    //playing it with looping
            .play(true);
	//2D engine to render the tiles/sprites
	Engine e = new Engine(theStage, 32 * 16, 32 * 16) {

            //Called after the graphics context creation and before the main loop
            @Override
            public void initialize(GraphicsContext gc){

            }

            //Called each main loop
	    @Override
	    public void update(GraphicsContext gc, double elapsedTime, List<String> inputs) {	
		sprite.render(gc);
		aSprite.render(gc);
	    }
	};

```

Loading a map from a TMX file (from Tiled):

```
#!java
        //Create the map from file 'tiledmap.tmx' (edited with Tiled software)
	TmxTileMap map = new TmxTileMap().loadFromFile("tiledmap.tmx");
```

Simply call map.update and map.render in the main loop to draw it.

### TODO ###

General:
* networking
* battle system
* save load
* ui menu
* inventory system (objects catching)
* ia
* use JavaFX animation api (interpolations)
* handling audio

Map:
* multi tileset
*animated tiles

Graphical optimizations:
* one d array
* background map image