package engine2d.map.tmx;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;

/**
 * <tileset firstgid="1" name="tilea1" tilewidth="32" tileheight="32" tilecount="1504" columns="8">
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class TileSet {

    /**
     * 
     */
    @XmlAttribute
    private int firstgid;
    
    /**
     * 
     */
    @XmlAttribute
    private String name;
    
    /**
     * 
     */
    @XmlAttribute
    private int tilewidth;
    
    /**
     * 
     */
    @XmlAttribute
    private int tileheight;
    
    /**
     * 
     */
    @XmlAttribute
    private int tilecount;
    
    /**
     * 
     */
    @XmlAttribute
    private int columns;
    
    /**
     * 
     */
    @XmlElement
    private Image image;
    
    /**
     * 
     */
    @XmlElements({
	@XmlElement(name = "tile", type = Tile.class),})
    private List<Tile> tiles = new ArrayList<>();

    /**
     * 
     * @return 
     */
    public int getFirstgid() {
	return firstgid;
    }

    /**
     * 
     * @return 
     */
    public String getName() {
	return name;
    }

    /**
     * 
     * @return 
     */
    public int getTilewidth() {
	return tilewidth;
    }

    /**
     * 
     * @return 
     */
    public int getTileheight() {
	return tileheight;
    }

    /**
     * 
     * @return 
     */
    public int getTilecount() {
	return tilecount;
    }

    /**
     * 
     * @return 
     */
    public int getColumns() {
	return columns;
    }

    /**
     * 
     * @return 
     */
    public Image getImage() {
	return image;
    }

    /**
     * 
     * @return 
     */
    public List<Tile> getTiles() {
	return tiles;
    }
    
    /**
     * 
     * @return 
     */
    public Tile getTileById(int tileId) throws MapException{
	for(Tile t:tiles){
	    if(t.getId()==tileId){
		return t;
	    }
	}
	throw new MapException("No tile object with id '"+tileId+"'");
    }
        
    /**
     * 
     * @return 
     */
    @Override
    public String toString(){
	StringBuilder s = new StringBuilder();
	s.append(this.name).append(" from ").append(this.image).append(" (").append(this.tilecount).append(" tiles)").append("\n");
	s.append("Tiles:\n");
	for(Tile t:tiles){
	    s.append(t).append("\n");
	}
	return s.toString();
    }    
}
