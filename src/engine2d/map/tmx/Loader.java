package engine2d.map.tmx;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Loader {

    /**
     * 
     * @param filename
     * @return
     * @throws JAXBException 
     */
    public static Map load(String filename) throws JAXBException {
	JAXBContext jaxbContext = JAXBContext.newInstance(Map.class);
	Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	Map map= (Map) jaxbUnmarshaller.unmarshal(new File(filename));
	return map;
    }
}
