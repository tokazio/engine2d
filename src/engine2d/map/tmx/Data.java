package engine2d.map.tmx;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * <data encoding="csv">
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Data {
    
    /**
     * 
     */
    @XmlAttribute
    private String encoding;
    
    /**
     * 
     */
    @XmlValue
    private String data;
    
    /**
     * 
     * @return 
     */
    public String[] csv() throws MapException{
	if(!encoding.equals("csv")){
	    throw new MapException("No CSV datas");
	}
	return data.split(",");
    }
}
