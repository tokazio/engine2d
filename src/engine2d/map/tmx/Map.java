package engine2d.map.tmx;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <map version="1.0" orientation="orthogonal" renderorder="left-up" width="128" height="128" tilewidth="32" tileheight="32" nextobjectid="58">
 * @author Romain PETIT <tokazio@esyo.net>
 */
@XmlRootElement
public class Map {

    /**
     * 
     */
    @XmlAttribute
    private String version;

    /**
     * 
     */
    @XmlAttribute
    private String orientation;

    /**
     * 
     */
    @XmlAttribute
    private String renderorder;

    /**
     * 
     */
    @XmlAttribute
    private int width;

    /**
     * 
     */
    @XmlAttribute
    private int height;

    /**
     * 
     */
    @XmlAttribute
    private int tilewidth;

    /**
     * 
     */
    @XmlAttribute
    private int tileheight;

    /**
     * 
     */
    @XmlAttribute
    private int nextobjectid;

    /**
     *
     */
    @XmlElementWrapper(name = "properties")
    @XmlElement(name = "property")
    private List<Property> properties = new ArrayList<>();

    /**
     * 
     */
    @XmlElements({
	@XmlElement(name = "tileset", type = TileSet.class),})
    private List<TileSet> tilesets = new ArrayList<>();

    /**
     * 
     */
    @XmlElements({
	@XmlElement(name = "layer", type = Layer.class),})
    private List<Layer> layers = new ArrayList<>();

    /**
     * 
     */
    @XmlElements({
	@XmlElement(name = "objectgroup", type = MapObjectGroup.class),})
    private List<MapObjectGroup> objects = new ArrayList<>();

    /**
     *
     * @param propertyName
     *
     * @return
     *
     * @throws engine2d.map.tmx.MapException
     */
    public Property getProperty(String propertyName) throws MapException {
	for (Property property : this.properties) {
	    if (property.getName().equals(propertyName)) {
		return property;
	    }
	}
	throw new MapException("No '" + propertyName + "' property");
    }

    /**
     *
     * @return
     */
    public int getWidth() {
	return width;
    }

    /**
     *
     * @return
     */
    public int getHeight() {
	return height;
    }

    /**
     *
     * @return
     */
    public String getVersion() {
	return version;
    }

    /**
     *
     * @return
     */
    public String getOrientation() {
	return orientation;
    }

    /**
     *
     * @return
     */
    public String getRenderOrder() {
	return renderorder;
    }

    /**
     *
     * @return
     */
    public int getTileWidth() {
	return tilewidth;
    }

    /**
     *
     * @return
     */
    public int getTileHeight() {
	return tileheight;
    }

    /**
     *
     * @return
     */
    public int getNextObjectId() {
	return nextobjectid;
    }

    /**
     * 
     * @return 
     */
    public List<Property> getProperties() {
	return properties;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
	StringBuilder s = new StringBuilder();
	s.append("Map version: ").append(this.version).append("\n");
	s.append("Map orientation: ").append(this.orientation).append("\n");
	s.append("Map render order: ").append(this.renderorder).append("\n");
	s.append("Map next object id: ").append(this.nextobjectid).append("\n");
	s.append("Map tile size: ").append(this.tilewidth).append(",").append(this.tileheight).append("\n");
	s.append("Map size: ").append(this.getWidth()).append(",").append(this.getHeight()).append("\n");;
	s.append("Map tile sets:\n");
	for(TileSet ts:tilesets){
	    s.append(ts).append("\n");
	}
	s.append("Map layers:\n");
	for(Layer l:layers){
	    s.append(l).append("\n");
	}
	s.append("Map objects:\n");
	s.append(objects);
	return s.toString();
    }

    /**
     *
     * @param index
     *
     * @return
     */
    public TileSet getTileSet(int index) {
	return this.tilesets.get(index);
    }

    /**
     *
     * @return
     */
    public TileSet getTileSetByName(String tileSetName) throws MapException {
	for(TileSet ts:tilesets){
	    if(ts.getName().equals(tileSetName)){
		return ts;
	    }
	}
	throw new MapException("No tile set named '"+tileSetName+"'");
    }
    
    /**
     * 
     * @return 
     */
    public List<Layer> getLayers(){
	return this.layers;
    }
    
    /**
     * 
     * @return 
     */
    public int countLayer(){
	return this.layers.size();
    }
    
    /**
     *
     * @param index
     *
     * @return
     */
    public Layer getLayer(int index) {
	return this.layers.get(index);
    }

    /**
     *
     * @return
     */
    public Layer getLayerByName(String layerName) throws MapException {
	for(Layer l:layers){
	    if(l.getName().equals(layerName)){
		return l;
	    }
	}
	throw new MapException("No layer named '"+layerName+"'");
    }
}
