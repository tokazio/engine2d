package engine2d.map.tmx;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * <image source="tilea1.png" width="256" height="6016"/>
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Image {

    /**
     *
     */
    @XmlAttribute
    private String source;

    /**
     *
     */
    @XmlAttribute
    private int width;

    /**
     *
     */
    @XmlAttribute
    private int height;

    /**
     *
     * @return
     */
    public String getSource() {
	return source;
    }

    /**
     *
     * @return
     */
    public int getWidth() {
	return width;
    }

    /**
     *
     * @return
     */
    public int getHeight() {
	return height;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
	return source + " (" + width + "," + height + ")";
    }
}
