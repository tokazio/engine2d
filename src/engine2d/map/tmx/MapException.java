package engine2d.map.tmx;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class MapException extends Exception {

    /**
     * 
     * @param s 
     */
    public MapException(String s) {
	super(s);
    }
    
}
