package engine2d.map.tmx;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
class MapObjectGroup {
    
    /**
     * 
     */
    @XmlAttribute
    private String draworder;
    
    /**
     * 
     */
    @XmlAttribute
    private String color;
    
    /**
     * 
     */
    @XmlAttribute
    private String name;
    
    /**
     * 
     */
    @XmlElements({
	@XmlElement(name = "object", type = MapObject.class),})
    private List<MapObject> objects = new ArrayList<>();

    /**
     * 
     * @return 
     */
    public int countObjects(){
	return this.objects.size();
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    public MapObject get(int index){	
	return (index>0 && index<countObjects()) ? objects.get(index) : null;
    }

    /**
     * 
     * @return 
     */
    public String getDraworder() {
	return draworder;
    }

    /**
     * 
     * @return 
     */
    public String getColor() {
	return color;
    }

    /**
     * 
     * @return 
     */
    public String getName() {
	return name;
    }

    /**
     * 
     * @return 
     */
    public List<MapObject> getObjects() {
	return objects;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public String toString(){
	StringBuilder s = new StringBuilder();
	s.append("Object group '").append(name).append("':\n");
	for(MapObject o:objects){
	    s.append(o).append("\n");
	}
	return s.toString();
    }
}
