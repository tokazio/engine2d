package engine2d.map.tmx;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

/**
 * <object id="0" x="0" y="12" width="32" height="20"/>
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class MapObject {

    /**
     *
     */
    @XmlAttribute
    private int id;

    /**
     *
     */
    @XmlAttribute
    private String type;
    
    /**
     *
     */
    @XmlAttribute
    private int x;

    /**
     *
     */
    @XmlAttribute
    private int y;

    /**
     *
     */
    @XmlAttribute
    private int width;

    /**
     *
     */
    @XmlAttribute
    private int height;

    /**
     *
     */
    @XmlElementWrapper(name = "properties")
    @XmlElement(name = "property")
    private List<Property> properties = new ArrayList<>();

    /**
     * 
     * @return 
     */
    public int getId() {
	return id;
    }

    /**
     * 
     * @return 
     */
    public String getType() {
	return type;
    }

    /**
     * 
     * @return 
     */
    public int getX() {
	return x;
    }

    /**
     * 
     * @return 
     */
    public int getY() {
	return y;
    }

    /**
     * 
     * @return 
     */
    public int getWidth() {
	return width;
    }

    /**
     * 
     * @return 
     */
    public int getHeight() {
	return height;
    }

    /**
     * 
     * @return 
     */
    public List<Property> getProperties() {
	return properties;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
	return "Object #" + this.id + " ["+type+"] position: " + this.x + "," + this.y + " size: " + this.width + "," + this.height;
    }
}
