package engine2d.map.tmx;

import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Tile {

    /**
     *
     */
    @XmlAttribute
    private int id;

    /**
     *
     */
    @XmlElement
    private MapObjectGroup objectgroup;

    /**
     *
     * @return
     */
    @Override
    public String toString() {
	return "Tile #" + id + ":\n" + objectgroup;
    }

    /**
     *
     * @return
     */
    public int getId() {
	return id;
    }
    
    /**
     * 
     * @return 
     */
    public List<MapObject> getObjects(){
	return objectgroup.getObjects();
    }    
}
