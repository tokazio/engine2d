package engine2d.core;

import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public interface OnRenderEvent {
    
    /**
     * 
     */
    void call(GraphicsContext gc);
}
