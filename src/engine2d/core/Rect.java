package engine2d.core;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Rect {

    /**
     *
     */
    private final double x;

    /**
     *
     */
    private final double y;

    /**
     *
     */
    private final double width;

    /**
     *
     */
    private final double height;

    /**
     *
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public Rect(double x, double y, double width, double height) {
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
    }

    /**
     *
     * @return
     */
    public double getX() {
	return x;
    }

    /**
     *
     * @return
     */
    public double getY() {
	return y;
    }

    /**
     *
     * @return
     */
    public double getWidth() {
	return width;
    }

    /**
     *
     * @return
     */
    public double getHeight() {
	return height;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
	return x + "," + y + " " + (x + width) + "," + (y + height);
    }

    /**
     *
     * @param x
     * @param y
     */
    public boolean intersect(double x, double y, double w, double h) {
	return intersect(new Rect(x,y,w,h));
    }
    
    /**
     * 
     * @param rect
     * @return 
     */
    public boolean intersect(Rect rect){
	return rect.getX() < this.x + this.width
		&& rect.getX() + rect.getWidth() > this.x
		&& rect.getY() < this.y + this.height
		&& rect.getHeight() + rect.getY() > this.y;
    }
}
