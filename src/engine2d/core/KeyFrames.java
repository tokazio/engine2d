package engine2d.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * List of frame #id for animate a sprite from a tileset.
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class KeyFrames {

    /**
     *
     */
    private final List<Integer> keys = new ArrayList<>();

    /**
     *
     * @param newKeys
     */
    public KeyFrames(Integer... newKeys) {
	this.add(newKeys);
    }
    
    /**
     * 
     * @param newKeys
     * @return 
     */
    public KeyFrames add(Integer newKey){
	this.keys.add(newKey);
	return this;
    }
    
    /**
     * 
     * @param newKeys
     * @return 
     */
    public final KeyFrames add(Integer... newKeys){
	this.keys.addAll(Arrays.asList(newKeys));
	return this;
    }

    /**
     *
     * @param id
     *
     * @return
     */
    public int getKey(int id) {
	return this.keys.get(id);
    }

    /**
     *
     * @return
     */
    public int count() {
	return this.keys.size();
    }
    
    /**
     * 
     * @return 
     */
    public KeyFrames clear(){
	this.keys.clear();
	return this;
    }
}
