package engine2d.core;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;

/**
 * Object that can be rendered on a GraphicsContext.
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public interface Renderable {

    /**
     * 
     * @param gc
     */
    void render(GraphicsContext gc);
    
    /**
     *
     * @param <T>
     * @param point
     * @return 
     */
    <T extends Renderable> T setPosition(Point2D point);
}
