package engine2d.core;

import javafx.scene.input.KeyEvent;

/**
 * Key handler based on Observer/Observable design pattern.
 * 
 * The keyable will implements the onKey meythod to handle key action.
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public interface Keyable {
    
    /**
     * 
     * @param k
     */
    void onKeyPress(KeyEvent k);
    
    /**
     * 
     * @param k
     */
    void onKeyUp(KeyEvent k);
}
