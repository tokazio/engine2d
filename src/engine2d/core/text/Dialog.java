package engine2d.core.text;

import engine2d.core.Animated;
import engine2d.core.AnimatedRect;
import engine2d.core.Keyable;
import engine2d.core.Renderable;
import engine2d.core.Updatable;
import engine2d.core.utils.OnEndEvent;
import java.util.List;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Dialog extends Animated implements Updatable, Renderable, Keyable {

    /**
     *
     */
    private final AnimatedRect ar;

    /**
     *
     */
    private final LetterByLetterText t;

    /**
     *
     * @param position
     * @param text
     */
    public Dialog(Point2D position, String text) {
	t = new LetterByLetterText(text, new Point2D(20, 385), new Point2D(482, 80));
	ar = new AnimatedRect(position.getX(), 10, position.getY(), 380, 0, 492, 0, 100, 500);
	ar.onEnd = new OnEndEvent() {

	    @Override
	    public void call() {
		if (isPlaying()) {
		    t.play();
		}
	    }

	};
    }

    /**
     *
     * @param loop
     *
     * @return
     */
    @Override
    public Dialog play(boolean loop) {
	ar.play(false);
	return super.play(loop);
    }

    /**
     *
     * @param elapsedTime
     */
    @Override
    public void update(long elapsedTime){
	t.update(elapsedTime);
	ar.update(elapsedTime);
    }

    /**
     *
     * @param gc
     */
    @Override
    public void render(GraphicsContext gc) {
	ar.render(gc);
	t.render(gc);
    }

    /**
     *
     * @param <T>
     * @param point
     *
     * @return
     */
    @Override
    public <T extends Renderable> T setPosition(Point2D point) {
	return (T) this;
    }

    /**
     * 
     */
    @Override
    public void onKeyPress(KeyEvent k) {
	t.onKeyPress(k);
    }
    
    /**
     * 
     */
    @Override
    public void onKeyUp(KeyEvent k) {
	t.onKeyUp(k);
    }
}
