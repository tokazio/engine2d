package engine2d.core;

import engine2d.core.utils.OnEndEvent;

/**
 * Object that can be animated (Animable)
 *
 * Animable implementation
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Animated implements Animable {

    /**
     *
     */
    protected boolean playing = false;

    /**
     *
     */
    protected boolean looping = false;

    /**
     *
     */
    public OnEndEvent onEnd = null;

    /**
     *
     * @return
     */
    @Override
    public boolean isPlaying() {
	return playing;
    }

    /**
     *
     * @param <T>
     * @param loop
     *
     * @return
     */
    @Override
    public <T extends Animable> T play(boolean loop) {
	looping = loop;
	playing = true;
	return (T) this;
    }

    /**
     *
     * @param <T>
     *
     * @return
     */
    @Override
    public <T extends Animable> T pause() {
	playing = false;
	return (T) this;
    }

    /**
     *
     * @param <T>
     *
     * @return
     */
    @Override
    public <T extends Animable> T resume() {
	playing = true;
	return (T) this;
    }

    /**
     *
     * @param <T>
     *
     * @return
     */
    @Override
    public <T extends Animable> T stop() {
	playing = false;
	return (T) this;
    }

    /**
     *
     */
    @Override
    public <T extends Animable> T onEnd() {
	playing = false;
	if (onEnd != null) {
	    onEnd.call();
	}
	return (T) this;
    }
}
