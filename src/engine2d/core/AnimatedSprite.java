package engine2d.core;

import java.util.HashMap;
import java.util.Map;
import javafx.scene.canvas.GraphicsContext;

/**
 * Sprite playing multiple key frame from a tileset.
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class AnimatedSprite extends Sprite implements Animable {

    /**
     *
     */
    private static final boolean debug = false;

    /**
     *
     */
    private boolean playing;

    /**
     *
     */
    private boolean looping;

    /**
     *
     */
    private int playAtLeast = 0;

    /**
     *
     */
    protected int duration = 128;

    /**
     *
     */
    private long previousTime;

    /**
     *
     */
    protected int frame = 0;

    /**
     *
     */
    protected int loopFrame = 0;

    /**
     *
     */
    private final Map<String, KeyFrames> keyFrames = new HashMap<>();

    /**
     *
     */
    private String currentKeyFramesName = null;

    /**
     *
     * @param tile
     */
    public AnimatedSprite(TileSet tile) {
	super(tile, 0);
    }

    /**
     *
     * @param tiles
     * @param x
     * @param y
     */
    public AnimatedSprite(TileSet tiles, double x, double y) {
	super(tiles, 0, x, y);
    }

    /**
     *
     * @param <T>
     * @param looping
     *
     * @return chaining
     */
    @Override
    public <T extends Animable> T play(boolean looping) {
	if (!isPlaying()) {
	    this.looping = looping;
	    this.previousTime = System.currentTimeMillis();
	    this.playing = true;
	}
	return (T) this;
    }

    /**
     *
     * @param <T>
     *
     * @return
     */
    @Override
    public <T extends Animable> T resume() {
	return play(looping);
    }

    /**
     *
     * @param <T>
     *
     * @return
     */
    @Override
    public <T extends Animable> T pause() {
	if (isPlaying() && frame >= playAtLeast) {
	    this.playing = false;
	}
	return (T) this;
    }

    /**
     *
     * @param <T>
     *
     * @return
     */
    @Override
    public <T extends Animable> T stop() {
	if (isPlaying() && frame >= playAtLeast) {
	    this.playing = false;
	    this.frame = 0;
	}
	return (T) this;
    }

    /**
     *
     * @param <T>
     * @param name
     * @param animation
     *
     * @return chaining
     */
    public <T extends AnimatedSprite> T addKeyFrames(String name, KeyFrames animation) {
	this.addKeyFrames(name, animation, true);
	return (T) this;
    }

    /**
     *
     * @param <T>
     * @param name
     * @param animation
     * @param set
     *
     * @return
     */
    public <T extends AnimatedSprite> T addKeyFrames(String name, KeyFrames animation, boolean set) {
	this.keyFrames.put(name, animation);
	if (set) {
	    try {
		this.selectKeyFrames(name);
	    } catch (AnimatedSpriteException ex) {
		//We add it just before, then no AnimatedSpriteException possible here!
	    }
	}
	return (T) this;
    }

    /**
     *
     * @param name
     *
     * @return
     */
    public KeyFrames getKeyFrames(String name) {
	return this.keyFrames.get(currentKeyFramesName);
    }

    /**
     *
     * @return
     */
    public KeyFrames getSelectedKeyFrames() {
	return this.getKeyFrames(this.currentKeyFramesName);
    }

    /**
     *
     * @return
     */
    public String getSelectedKeyFramesName() {
	return this.currentKeyFramesName;
    }

    /**
     *
     * @param <T>
     * @param name
     *
     * @return
     *
     * @throws engine2d.core.AnimatedSpriteException
     */
    public <T extends AnimatedSprite> T selectKeyFrames(String name) throws AnimatedSpriteException {
	if (!this.keyFrames.containsKey(name)) {
	    throw new AnimatedSpriteException("Animation '" + name + "' doesn't exists. Choose in: " + this.keyFrames.keySet());
	}
	this.currentKeyFramesName = name;
	return (T) this;
    }

    /**
     *
     * @param <T>
     *
     * @return
     */
    @Override
    public <T extends Animable> T onEnd() {
	//@todo: call onEnd
	return (T) this;
    }

    /**
     *
     * @param i
     *
     * @return
     */
    public <T extends AnimatedSprite> T setPlayAtLeast(int i) {
	playAtLeast = i;
	return (T) this;
    }

    /**
     *
     * @return
     */
    protected int getFrame() {
	if (!this.keyFrames.isEmpty() && this.getSelectedKeyFramesName() != null) {
	    if (isPlaying()) {
		if (System.currentTimeMillis() - this.previousTime > this.duration) {
		    this.frame++;
		    if (this.frame > this.getSelectedKeyFrames().count() - 1) {
			if (this.looping) {
			    this.frame = loopFrame;
			} else {
			    this.playing = false;
			    this.frame--;
			}
		    }
		    this.previousTime = System.currentTimeMillis();
		}
	    } else {
		this.frame = 0;
	    }
	    return this.getSelectedKeyFrames().getKey(this.frame);
	} else {
	    return this.getDefaultTileCode();
	}
    }

    /**
     *
     * @param gc
     */
    @Override
    public void render(GraphicsContext gc) {
	this.getTileSet().render(gc, this.getFrame(), this.getPosition().getX(), this.getPosition().getY());
	if (debug) {
	    gc.strokeText(frame + "", this.getPosition().getX(), this.getPosition().getY() + 20);
	}
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isPlaying() {
	return this.playing;
    }

    /**
     *
     * @return
     */
    public boolean isLopped() {
	return this.looping;
    }

    /**
     *
     * @param <T>
     * @param duration
     *
     * @return
     */
    public <T extends AnimatedSprite> T setDuration(int duration) {
	this.duration = duration;
	return (T) this;
    }

    /**
     *
     * @param <T>
     * @param frame
     *
     * @return
     */
    public <T extends AnimatedSprite> T setLoopFrame(int frame) {
	this.loopFrame = frame;
	return (T) this;
    }
}
