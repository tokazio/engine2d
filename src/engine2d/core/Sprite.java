package engine2d.core;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;

/**
 * Sprite (static) from a tileset.
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Sprite implements Renderable {

    /**
     *
     */
    private final TileSet tiles;
    
    /**
     * 
     */
    protected final int defaultTileCode;

    /**
     *
     */
    protected double positionX;
    
    /**
     * 
     */
    protected double positionY;

    /**
     *
     * @param tiles
     * @param tileCode
     */
    public Sprite(TileSet tiles, int tileCode) {
	this.tiles = tiles;
	this.defaultTileCode = tileCode;
	positionX = 0;
	positionY = 0;
    }
    
    /**
     *
     * @param tiles
     * @param tileCode
     * @param x
     * @param y
     */
    public Sprite(TileSet tiles, int tileCode, double x, double y) {
	this(tiles,tileCode);
	positionX = x;
	positionY = y;
    }

    /**
     * 
     * @return 
     */
    public int getDefaultTileCode(){
	return this.defaultTileCode;
    }
    
    /**
     * 
     * @return 
     */
    public TileSet getTileSet(){
	return this.tiles;
    }
    
    /**
     * 
     * @return 
     */
    public Point2D getPosition(){
	return new Point2D(positionX,positionY);
    }
    
    /**
     *
     * @param point
     */
    @Override
    public <T extends Renderable> T setPosition(Point2D point) {
	this.positionX = point.getX();
	this.positionY = point.getY();	
	return (T) this;
    }
    
    /**
     *
     * @param <T>
     * @param x
     * @param y
     * @return 
     */
    public <T extends Renderable> T setPosition(double x, double y) {
	this.positionX = x;
	this.positionY = y;	
	return (T) this;
    }
    
    /**
     *
     * @param gc
     */
    @Override
    public void render(GraphicsContext gc) {
	this.tiles.render(gc, this.getDefaultTileCode(), this.positionX, this.positionY);
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public String toString(){
	return "#"+this.getDefaultTileCode()+" @ "+this.positionX+","+this.positionY;
    }
}
