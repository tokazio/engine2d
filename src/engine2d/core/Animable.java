package engine2d.core;

/**
 * Use on object that can be animated.
 * Play, pause, resume, stop animation.
 * Give onEnd callback event after the animation.
 * 
 * See Animated class as direct implementation of this interface.
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public interface Animable {

    /**
     *
     * @param <T>
     * @param loop
     *
     * @return
     */
    <T extends Animable> T play(boolean loop);

    /**
     *
     * @param <T>
     *
     * @return
     */
    <T extends Animable> T pause();

    /**
     *
     * @param <T>
     *
     * @return
     */
    <T extends Animable> T resume();

    /**
     *
     * @param <T>
     *
     * @return
     */
    <T extends Animable> T stop();

    /**
     *
     * @param <T>
     *
     * @return
     */
    <T extends Animable> T onEnd();
         
    /**
     * 
     * @return 
     */
    boolean isPlaying();
}
